/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

// You can delete this file if you're not using it
const path = require(`path`)
const { createFilePath } = require(`gatsby-source-filesystem`)
const { fmImagesToRelative } = require('gatsby-remark-relative-images')

exports.createPages = async ({ actions, graphql, reporter }) => {
  const { createPage } = actions;

  const projectPageTemplate = path.resolve(`src/templates/ProjectPage.js`);
  const aboutPageTemplate = path.resolve(`src/templates/AboutPage.js`);

  const projectsQuery = await graphql(
    `{
      allFile(filter: {internal: {mediaType: {eq: "text/markdown"}}, sourceInstanceName: {eq: "projects"}}) {
        edges {
          node {
            childMarkdownRemark {
              fields {
                slug
              }
              frontmatter {
                title
              }
            }
          }
        }
      }
    }`
  )

  // Handle errors
  if (projectsQuery.errors) {
    reporter.panicOnBuild(`Error while running PROJECTS GraphQL query.`)
    return
  }

  const pagesQuery = await graphql(
    `{
        allFile(filter: {internal: {mediaType: {eq: "text/markdown"}}, sourceInstanceName: {eq: "pages"}}) {
          edges {
            node {
              name
              childMarkdownRemark {
                html
                fields {
                  slug
                }
              }
            }
          }
        }
      }`
  )

  // Handle errors
  if (pagesQuery.errors) {
    reporter.panicOnBuild(`Error while running PAGES GraphQL query.`)
    return
  }

  const projects = projectsQuery.data.allFile.edges
  const pages = pagesQuery.data.allFile.edges

  projects.forEach((project, index) => {
    const previous = index === projects.length - 1 ? null : projects[index + 1].node
    const next = index === 0 ? null : projects[index - 1].node

    createPage({
      path: project.node.childMarkdownRemark.fields.slug,
      component: projectPageTemplate,
      context: {
        slug: project.node.childMarkdownRemark.fields.slug,
        previous,
        next,
      },
    })
  })

  pages.forEach((page) => {
    createPage({
      path: page.node.name,
      component: aboutPageTemplate,
      context: {
        slug: page.node.childMarkdownRemark.fields.slug
      }
    })
  })
}

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions

  fmImagesToRelative(node)

  if (node.internal.type === `MarkdownRemark`) {
    const value = createFilePath({ node, getNode })
    createNodeField({
      name: `slug`,
      node,
      value: node.frontmatter.layout === 'project' ? `/projects${value}` : value,
    }) 
  }
}
