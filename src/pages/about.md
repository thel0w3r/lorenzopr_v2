---
layout: page
title: Lorenzo Pinna Rodríguez
profilePic: /uploads/profile_photo.jpg
description: >-
  I would define myself as someone who's passionate about technology,
  specifically software engineering, since I was 12, I started being interested
  in computers, at that time, my interest was more about computer security than
  programming.

  Years later, I started learning Visual Basic by myself, then I moved through
  different languages and frameworks, like Pascal with Delphi and C++ with some
  OpenGL code.

  Right now, I'm more into multiplatform development, I have personal projects
  written in Java, C#, JavaScript, Dart, etc.

  My plans for the future are to learn as much as I can, go deeper into the
  things I know and explore other areas in software development.

  Almost everything I've learned had been in a self taught way, but a lot of
  refinements and tips came from other experienced people I've had the luck to
  work with.
skills:
  languages:
    - amount: 99
      name: HTML/CSS
    - amount: 90
      name: Java
    - amount: 85
      name: 'C#'
    - amount: 80
      name: JavaScript
    - amount: 65
      name: PHP
    - amount: 30
      name: C++
  technologies:
    - amount: 80
      name: Android
    - amount: 76
      name: Vue
    - amount: 70
      name: React
    - amount: 60
      name: Flutter
    - amount: 55
      name: Unity
    - amount: 50
      name: Angular
---

