---
layout: project
title: HPWizard
thumbnail: /uploads/hpwizard_logo.png
technologies:
  - Java
  - Spigot
description: HPWizard is a role-playing plugin for Spigot. Is written in Java and using the Bukkit API.
---
## About HPWizard

HPWizard is a role-playing plugin for Spigot. Is written in Java and using the Bukkit API.

Some of it's features are:

- Wands
- Spells
- Brooms
- Floo Network
- Sorting Hat
- House points
- Custom house colors
- Custom house names
- House point signs
- Custom spell sounds

## Showcase Video

`youtube: https://www.youtube.com/watch?v=KgWedADcPm4` 
