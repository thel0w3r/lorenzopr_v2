---
layout: project
title: TLGram
thumbnail: /uploads/tlgram.png
technologies:
  - JavaScript
  - React
  - Firebase
  - Material-UI
description: TLGram is a clone of Instagram made for learning purposes.
---
## About TLGram

TLGram is a clone of Instagram made for learning purposes.

Is written in Javascript using [ReactJS](https://reactjs.org/) and [Material-UI](https://material-ui.com) for it's presentation components, although some of them are custom made too.

For it's backend, it's currently using Firebase and Firebase Realtime Database.

It currently packs features like:

- Registration
- Logging In
- Uploading photos
- Searching other users
- Viewing users profiles
- More features coming...