module.exports = {
  siteMetadata: {
    title: 'Lorenzo Pinna Rodríguez, junior Web & Mobile developer from Gran Canaria.',
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: 'lorenzopr',
        short_name: 'lpr',
        start_url: '/',
        background_color: '#282A2C',
        theme_color: '#282A2C',
        display: 'browser',
        icon: 'src/images/sitelogo.svg', // This path is relative to the root of the site.
      },
    },
    `gatsby-plugin-styled-components`,
    {
      resolve: 'gatsby-plugin-web-font-loader',
      options: {
        google: {
          families: ['Roboto:100,300,400,500']
        }
      }
    },
    'gatsby-plugin-offline',
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/static/uploads`,
        name: 'uploads',
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/src/pages/projects`,
        name: 'projects',
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/src/pages`,
        name: 'pages',
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          {
            resolve: `gatsby-remark-relative-images`,
            options: {
              name: "uploads" // Must match the source name ^
            }
          },
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 300,
              showCaptions: false,
            },
          },
          {
            resolve: "gatsby-remark-embed-video",
            options: {
              width: 480,
              ratio: 1.77,
              related: false, //Optional: Will remove related videos from the end of an embedded YouTube video.
              noIframeBorder: true //Optional: Disable insertion of <style> border: 0
            }
          },
          `gatsby-remark-responsive-iframe`
        ],
      },
    },
    `gatsby-plugin-netlify-cms`,
    `gatsby-plugin-sass`,
    `gatsby-plugin-dark-mode`,
    {
      resolve: 'gatsby-plugin-modal-routing',
      options: {
        modalProps: {
          bodyOpenClassName: null,
          closeTimeoutMS: 300,
          styles: { overlay: null, content: null }
        }
      }
    },
    `gatsby-plugin-netlify`
  ],
}
